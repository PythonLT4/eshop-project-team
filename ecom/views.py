from django.http import HttpRequest
from django.shortcuts import render

from ecom.services import get_product_list


def product_list(request: HttpRequest):
    # TODO: https://gitlab.com/PythonLT4/eshop-project-team/-/issues/2
    return render(request, 'product_list.html', {
        'products': get_product_list(),
    })

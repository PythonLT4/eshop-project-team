import pytest

from ecom.models import Product
from ecom.services import get_product_list


@pytest.mark.django_db
def test_get_product_list_empty():
    assert list(get_product_list()) == []


@pytest.mark.django_db
def test_get_product_list():
    p1 = Product.objects.create(title='Product 1')
    assert list(get_product_list()) == [p1]


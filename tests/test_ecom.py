import pytest
from django.test import Client

from ecom.models import Product


@pytest.mark.django_db
def test_index(client: Client):
    resp = client.get('/')
    assert resp.status_code == 200
    assert list(resp.context['products']) == []

@pytest.mark.django_db
def test_product_list(client: Client):
    p1 = Product.objects.create(title='Product 1')
    resp = client.get('/')
    assert resp.status_code == 200
    assert list(resp.context['products']) == [p1]